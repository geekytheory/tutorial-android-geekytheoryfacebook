package com.example.geekytheoryfacebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class MainActivity extends Activity implements OnClickListener {

	//declaramos los elementos del XML
	EditText etTitulo, etDescripcion, etCaption, etUrl, etLink;
	Button publicar;
	
	//Guardamos en una constante el ID de nuestra app de Facebook
	 private static String ID_FB="145396652309556";
	//creamos un objeto Facebook
	 private Facebook facebook;
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	//Creamos y asignamos los objetos de nuestro XML
		etTitulo = (EditText) findViewById(R.id.etTitulo);
		etDescripcion = (EditText) findViewById(R.id.etDescripcion);
		etCaption = (EditText) findViewById(R.id.etCaption);
		etUrl = (EditText) findViewById(R.id.etUrl);
		etLink = (EditText) findViewById(R.id.etLink);
		
		publicar = (Button)findViewById(R.id.bPost);
		publicar.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==publicar.getId()){
			//al pulsar sobre el boton publicar invocamos el m�todo publicaMuro()
			publicarMuro();	
		}		
	}
	
	private void publicarMuro() {
		// TODO Auto-generated method stub
			
		//Creamos un objeto facebook con el id de nuestra app
			facebook = new Facebook(ID_FB);
		//Ejecutamos la sesion de facebook	
	        new AsyncFacebookRunner(facebook);
	        
	    //en funci�n del estado de la sesi�n hacemos una cosa u otra
	        if(!facebook.isSessionValid()) {
	        	//si la sesion es valida mostramos en pantalla que hemos accedido correctamente y
	        	//publicamos el mensaje con el m�todo mensaje_en_el_muro()
	            facebook.authorize(this, new String[] {}, new DialogListener() {
	                @Override
	                
	          
	                public void onComplete(Bundle values) {
	                	
	            		Toast mensaje= Toast.makeText(MainActivity.this,"Has iniciado la sesion correctamente.", Toast.LENGTH_SHORT);
	            		mensaje.show();
	            		//escribimos el mensaje en el muro
	        			mensaje_En_El_Muro();
	                }
	              
	                //Si la sesion devuelve un error mostramos un mensaje de error

					@Override
	                public void onFacebookError(FacebookError error) {
	                	
	                	Toast mensaje= Toast.makeText(MainActivity.this,"Ha ocurrido un error al intentar iniciar sesi�n", Toast.LENGTH_SHORT);
	            		mensaje.show();
	                }
	                
	                @Override
	                public void onError(DialogError e) {
	                	
	                	Toast mensaje= Toast.makeText(MainActivity.this,"Ha ocurrido un error al intentar iniciar sesi�n", Toast.LENGTH_SHORT);
	            		mensaje.show();
	                }
	                
	                @Override
	                public void onCancel() {}
	            });
	        }	       
	    }
	
    private void mensaje_En_El_Muro() {
		// TODO Auto-generated method stub
    	
    	Bundle params = new Bundle();
		
    	//introducimos dentro del objeto Bundle el texto que introduzcamos en los editText.
		params.putString("description", etDescripcion.getText()+"");
	    params.putString("name", etTitulo.getText()+"");
	    params.putString("caption", etCaption.getText()+"");
	    params.putString("picture", etUrl.getText()+"");
	    params.putString("link", etLink.getText()+"");
	    //ejecutamos un dialogo con el objeto Bundle rellenado
	    facebook.dialog(MainActivity.this, "feed", params, new SampleDialogListener());
	}
    
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        facebook.authorizeCallback(requestCode, resultCode, data);
    }

    public class SampleDialogListener extends BaseDialogListener {

        public void onComplete(Bundle values) {
        	Toast mensaje= Toast.makeText(MainActivity.this,"Enviado", Toast.LENGTH_SHORT);
    		mensaje.show();
        }
    }
}